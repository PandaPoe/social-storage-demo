package com.mive.android.socialstorageapplication;

import com.mive.android.socialstorageapplication.enums.FilesSortingModeEnum;
import com.mive.android.socialstorageapplication.models.AmazonFile;

import java.util.Comparator;

public class AmazonFilesComparator implements Comparator<AmazonFile> {

    private FilesSortingModeEnum mode;

    public AmazonFilesComparator() {
        mode = FilesSortingModeEnum.AscendingByName;
    }

    public AmazonFilesComparator(FilesSortingModeEnum mode) {
        this.mode = mode;
    }

    @Override
    public int compare(AmazonFile o1, AmazonFile o2) {
        if (mode.equals(FilesSortingModeEnum.AscendingByName))
            return o1.getFilePath().compareToIgnoreCase(o2.getFilePath());
        else if (mode.equals(FilesSortingModeEnum.DescendingByName))
            return o2.getFilePath().compareToIgnoreCase(o1.getFilePath());
        else if (mode.equals(FilesSortingModeEnum.AscendingByDate))
            return o1.getLastModified().compareTo(o2.getLastModified());
        else
            return o2.getLastModified().compareTo(o1.getLastModified());
    }

    @Override
    public boolean equals(Object obj) {
        return false;
    }

    public void setSortingMode(FilesSortingModeEnum mode) {
        this.mode = mode;
    }
}
