package com.mive.android.socialstorageapplication.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.mive.android.socialstorageapplication.DownloadingQueueObserver;
import com.mive.android.socialstorageapplication.R;
import com.mive.android.socialstorageapplication.StorageObserver;
import com.mive.android.socialstorageapplication.adapters.SocialStorageArrayAdapter;
import com.mive.android.socialstorageapplication.enums.FileCategoryLoadEnum;
import com.mive.android.socialstorageapplication.enums.FileOperationTypeEnum;
import com.mive.android.socialstorageapplication.enums.FilesSortingModeEnum;
import com.mive.android.socialstorageapplication.models.AmazonFile;
import com.mive.android.socialstorageapplication.models.SynchronizationMessage;
import com.mive.android.socialstorageapplication.services.AmazonFilesStorageService;
import com.mive.android.socialstorageapplication.workers.SocialStorageLoader;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import at.grabner.circleprogress.CircleProgressView;

public class SocialStorageContentFragment extends Fragment implements LoaderManager.LoaderCallbacks<List<AmazonFile>> {

    private SocialStorageArrayAdapter socialStorageAdapter;

    private ListView booksListView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private TextView mErrorTextView;
    private ImageView imageStub;

    //private SocialStorageLoader loaderInstance;

    private View view;

    private FileCategoryLoadEnum mFileCategoryLoadType;

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static SocialStorageContentFragment newInstance(FileCategoryLoadEnum mfileCategoryLoadType) {
        SocialStorageContentFragment fragment = new SocialStorageContentFragment();
        fragment.setFileCategoryLoadType(mfileCategoryLoadType);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_social_storage_content, container, false);

        mErrorTextView = view.findViewById(R.id.errorMessage);
        mErrorTextView.setVisibility(View.GONE);

        booksListView = view.findViewById(R.id.filesList);
        mSwipeRefreshLayout = view.findViewById(R.id.swipeToRefresh);
        socialStorageAdapter = new SocialStorageArrayAdapter(getActivity(), new ArrayList<AmazonFile>(), mFileCategoryLoadType);

        Parcelable state = booksListView.onSaveInstanceState();

        booksListView.setAdapter(socialStorageAdapter);

        booksListView.onRestoreInstanceState(state);

        registerForContextMenu(booksListView);

        StorageObserver.SaveAdapterState(mFileCategoryLoadType, socialStorageAdapter);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                startFileListLoaderProcess(false);
            }
        });

        imageStub = view.findViewById(R.id.categoryImage);

        switch (mFileCategoryLoadType){
            case AllFiles:
                imageStub.setImageResource(R.drawable.ic_all_01);
                break;
            case LocalFilesOnly:
                imageStub.setImageResource(R.drawable.ic_offline_01);
                break;
            case FavoriteFilesOnly:
                imageStub.setImageResource(R.drawable.ic_fav_01);
                break;
        }

        booksListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, final View view, final int position, long id) {
                AmazonFile itemAtPosition = (AmazonFile) parent.getItemAtPosition(position);
                AmazonFilesStorageService amazonFilesStorageService = AmazonFilesStorageService.getInstance(getActivity());

            if (DownloadingQueueObserver.isLoading(itemAtPosition.getFilePath()))
                return;
            amazonFilesStorageService.setDownloadProgressListener(new TransferListener() {
                 AmazonFile itemAtPosition = (AmazonFile) parent.getItemAtPosition(position);
                 AmazonFilesStorageService amazonFilesStorageService = AmazonFilesStorageService.getInstance(getActivity());
                 CircleProgressView fileDownloadProgressView = view.findViewById(R.id.fileDownloadProgress);

                    @Override
                    public void onStateChanged(int id, TransferState state) {
                        if (state == TransferState.IN_PROGRESS) {
                            DownloadingQueueObserver.set(itemAtPosition.getFilePath(), 0);
                            fileDownloadProgressView.setValue(0);
                            fileDownloadProgressView.setSeekModeEnabled(false);
                            fileDownloadProgressView.setText("");
                            fileDownloadProgressView.setVisibility(View.VISIBLE);
                            fileDownloadProgressView.spin();
                        } else if (state == TransferState.COMPLETED) {
                            fileDownloadProgressView.setVisibility(View.GONE);
                            itemAtPosition.setSynchronized(true);
                            fileDownloadProgressView.stopSpinning();
                            ((SocialStorageArrayAdapter) parent.getAdapter()).updateValue(position, itemAtPosition);
                            DownloadingQueueObserver.remove(itemAtPosition.getFilePath());
                            EventBus.getDefault().post(new SynchronizationMessage(itemAtPosition, mFileCategoryLoadType, FileOperationTypeEnum.Synchronize));
                            this.amazonFilesStorageService.openDownloadedFile(itemAtPosition.getFilePath());
                        }
                    }

                    @Override
                    public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                        float percentage = (bytesCurrent / (float) bytesTotal * 100);
                        DownloadingQueueObserver.set(itemAtPosition.getFilePath(), (int)percentage);
                        fileDownloadProgressView.setValueAnimated(percentage);
                    }

                    @Override
                    public void onError(int id, Exception ex) {
                        Log.e("error", "error");
                    }
                });

                amazonFilesStorageService.downloadFileFromStorage(itemAtPosition.getFilePath());
            }
        });

        startFileListLoaderProcess(false);

        return view;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(SynchronizationMessage event) {
            startFileListLoaderProcess(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @NonNull
    @Override
    public Loader<List<AmazonFile>> onCreateLoader(int i, @Nullable Bundle bundle) {
        return new SocialStorageLoader(getActivity(), mFileCategoryLoadType);
    }

    @NonNull
    @Override
    public void onLoadFinished(@NonNull Loader<List<AmazonFile>> loader, final List<AmazonFile> amazonFiles) {

        socialStorageAdapter.setValues(amazonFiles);

        (new Handler()).postDelayed(new Runnable() {
            public void run() {
                if (socialStorageAdapter.isEmpty()) {
                    imageStub.setVisibility(View.VISIBLE);
                    //mErrorTextView.setVisibility(View.VISIBLE);
                } else {
                    imageStub.setVisibility(View.GONE);
                    //mErrorTextView.setVisibility(View.GONE);
                }
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }, 300);
    }

    @NonNull
    @Override
    public void onLoaderReset(@NonNull Loader<List<AmazonFile>> loader) {
        mSwipeRefreshLayout.setRefreshing(false);
        socialStorageAdapter.setValues(new ArrayList<AmazonFile>());
    }

    private void startFileListLoaderProcess(boolean silentUpdate) {
        try {
            mSwipeRefreshLayout.setRefreshing(!silentUpdate);
            imageStub.setVisibility(View.GONE);
            //mErrorTextView.setVisibility(View.GONE);

            LoaderManager.getInstance(this).initLoader(1, null, this).forceLoad();
        } catch (Exception ex) {
            mSwipeRefreshLayout.setRefreshing(false);
        }

    }

    public void setFileCategoryLoadType(FileCategoryLoadEnum mfileCategoryLoadType) {
        this.mFileCategoryLoadType = mfileCategoryLoadType;
    }
}
