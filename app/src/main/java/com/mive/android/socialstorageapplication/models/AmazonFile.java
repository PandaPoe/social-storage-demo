package com.mive.android.socialstorageapplication.models;


import java.util.Date;

public class AmazonFile {

    private String filePath;
    private String fileName;

    private boolean isSynchronized;
    private boolean isFavorite;
    private Date lastModified;
    private long fileSize;

    public AmazonFile(String filePath, Boolean isSynchronized, Date lastModified, long fileSize) {
        String[] paths = filePath.split("/");

        setFilePath(filePath);
        setFileName(paths[paths.length - 1]);
        setSynchronized(isSynchronized);
        setLastModified(lastModified);
        setFileSize(fileSize);
    }

    public AmazonFile() {

    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String fileName) {
        this.filePath = fileName;
    }

    public Boolean isSynchronized() {
        return isSynchronized;
    }

    public void setSynchronized(Boolean aSynchronized) {
        isSynchronized = aSynchronized;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public Boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favourite) {
        isFavorite = favourite;
    }

    @Override()
    public boolean equals(Object other) {
        if (other instanceof AmazonFile) {
            return getFileName().equals(((AmazonFile) other).getFileName()) &&
                    isSynchronized().equals(((AmazonFile) other).isSynchronized()) &&
                    isFavorite().equals(((AmazonFile) other).isFavorite());
        }
        return false;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}

