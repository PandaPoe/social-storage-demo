package com.mive.android.socialstorageapplication.workers;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.AsyncTaskLoader;

import java.util.List;

import com.mive.android.socialstorageapplication.enums.FileCategoryLoadEnum;
import com.mive.android.socialstorageapplication.models.AmazonFile;
import com.mive.android.socialstorageapplication.services.AmazonFilesStorageService;

public class SocialStorageLoader extends AsyncTaskLoader<List<AmazonFile>> {
    private AmazonFilesStorageService storageService;
    private FileCategoryLoadEnum mFileCategoryLoadType;

    public SocialStorageLoader(@NonNull Context context, FileCategoryLoadEnum mFileCategoryLoadType) {
        super(context);
        this.storageService = new AmazonFilesStorageService(context);
        this.mFileCategoryLoadType = mFileCategoryLoadType;
    }

    @Override
    public List<AmazonFile> loadInBackground() {

        List<AmazonFile> bucketFileListResult = storageService.getFilesFromStorage(mFileCategoryLoadType);

        return bucketFileListResult;
    }
}
