package com.mive.android.socialstorageapplication.models;

import com.mive.android.socialstorageapplication.enums.FileCategoryLoadEnum;
import com.mive.android.socialstorageapplication.enums.FileOperationTypeEnum;

public class SynchronizationMessage {
    public final AmazonFile file;
    public final FileCategoryLoadEnum sender;
    public final FileOperationTypeEnum operation;

    public SynchronizationMessage(AmazonFile file, FileCategoryLoadEnum sender, FileOperationTypeEnum operation) {
        this.file = file;
        this.sender = sender;
        this.operation = operation;
    }
}
