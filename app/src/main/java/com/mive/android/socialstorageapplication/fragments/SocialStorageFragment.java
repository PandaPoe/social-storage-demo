package com.mive.android.socialstorageapplication.fragments;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.view.menu.MenuItemImpl;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.mive.android.socialstorageapplication.R;
import com.mive.android.socialstorageapplication.adapters.SectionsPagerAdapter;
import com.mive.android.socialstorageapplication.adapters.SocialStorageArrayAdapter;
import com.mive.android.socialstorageapplication.enums.FilesSortingModeEnum;
import com.mive.android.socialstorageapplication.listeners.ToolbarMenuClickListener;

public class SocialStorageFragment extends Fragment {

    private View view;

    public static SocialStorageFragment newInstance() {
        return new SocialStorageFragment();
    }

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ToolbarMenuClickListener sortingMenuClickListener;

    //private ToolbarMenuClickListener sortingMenuClickListener;
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private TabLayout tabLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_social_storage, container, false);


        setHasOptionsMenu(false);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(this.getChildFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = view.findViewById(R.id.container);
        mViewPager.setOffscreenPageLimit(2);
        mViewPager.setAdapter(mSectionsPagerAdapter);


        tabLayout = view.findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.getMenu().clear();

        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);

        toolbar.inflateMenu(R.menu.menu_social_storage);
        setHasOptionsMenu(true);


        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_social_storage, menu);

        sortingMenuClickListener = new ToolbarMenuClickListener(getContext());
        sortingMenuClickListener.setSortingItemClickListener(new ToolbarMenuClickListener.SortingItemClickListener() {
            @Override
            public void sortByNameAscending() {
                sortItemsForSelectedPage(FilesSortingModeEnum.AscendingByName);
            }

            @Override
            public void sortByNameDescending() {
                sortItemsForSelectedPage(FilesSortingModeEnum.DescendingByName);
            }

            @Override
            public void sortByDateAscending() {
                sortItemsForSelectedPage(FilesSortingModeEnum.AscendingByDate);
            }

            @Override
            public void sortByDateDescending() {
                sortItemsForSelectedPage(FilesSortingModeEnum.DescendingByDate);
            }
        });

        menu.findItem(R.id.action_settings).setOnMenuItemClickListener(sortingMenuClickListener);


        super.onCreateOptionsMenu(menu, menuInflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.application_settings) {
            Toast.makeText(getContext(),"Social storage menu item", Toast.LENGTH_LONG).show();
        }

        return super.onOptionsItemSelected(item);
    }

    private void sortItemsForSelectedPage (FilesSortingModeEnum sortingType) {
        TabLayout layout = getActivity().findViewById(R.id.tabs);
        int tabPosition = layout.getSelectedTabPosition();

        ListView filesView = ((ViewPager)view
                .findViewById(R.id.container))
                .getChildAt(tabPosition)
                .findViewById(R.id.filesList);

        ((SocialStorageArrayAdapter)filesView.getAdapter()).sortFiles(sortingType);
    }

}
