package com.mive.android.socialstorageapplication.enums;

public enum FileOperationTypeEnum {
    Synchronize,
    CompletelyRemove,
    Remove,
    Favorite,
    NotFavorite,
}
