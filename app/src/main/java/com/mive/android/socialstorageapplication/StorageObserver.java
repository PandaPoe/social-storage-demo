package com.mive.android.socialstorageapplication;

import com.mive.android.socialstorageapplication.adapters.SocialStorageArrayAdapter;
import com.mive.android.socialstorageapplication.enums.FileCategoryLoadEnum;

import java.util.HashMap;
import java.util.Map;

public class StorageObserver {

    private static Map<String, SocialStorageArrayAdapter> storage = new HashMap<String, SocialStorageArrayAdapter>();


    public static void SaveAdapterState(FileCategoryLoadEnum loadCategory, SocialStorageArrayAdapter adapter) {
        storage.put(String.valueOf(loadCategory), adapter);
    }

    public static SocialStorageArrayAdapter GetAdapterByIndex(FileCategoryLoadEnum loadCategory) {
        return storage.get(String.valueOf(loadCategory));
    }
}
