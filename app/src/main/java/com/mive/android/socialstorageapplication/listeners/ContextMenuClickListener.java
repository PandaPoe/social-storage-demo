package com.mive.android.socialstorageapplication.listeners;

import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.View.OnClickListener;

import com.cocosw.bottomsheet.BottomSheet;
import com.mive.android.socialstorageapplication.R;
import com.mive.android.socialstorageapplication.models.AmazonFile;

public class ContextMenuClickListener implements OnClickListener {
    private AmazonFile mAmazonFile;
    private Context mContext;
    private FileItemClickListener mFileItemClickListener;

    public ContextMenuClickListener(Context context, AmazonFile file) {
        mContext = context;
        mAmazonFile = file;
    }

    @Override
    public void onClick(View v) {
        BottomSheet.Builder fileActionsSheetBuilder =  new BottomSheet.Builder(mContext, R.style.BottomSheet_Dialog)
            .title("File actions")
            .sheet(R.menu.menu_file_context_options)
            .listener(new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                Handler mainHandler = new Handler(mContext.getMainLooper());
                switch (which) {
                    case R.id.add_to_favourites:
                        mainHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (mFileItemClickListener!=null)
                                    mFileItemClickListener.onFileAddedToFavorites(mAmazonFile);
                            }
                        });
                        break;

                    case R.id.remove_favourites:
                        mainHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (mFileItemClickListener!=null) {
                                    mFileItemClickListener.onFileRemovedFromFavorites(mAmazonFile);
                                }
                            }
                        });
                        break;

                    case R.id.remove_from_device:
                        mainHandler.post(new Runnable() {
                            @Override
                            public void run() {

                                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

                                builder.setTitle(R.string.delete_header);

                                builder.setMessage(R.string.delete_local_message);
                                builder.setNegativeButton("Cancel", null);
                                builder.setPositiveButton("Delete", new DialogInterface.OnClickListener(){

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (mFileItemClickListener!=null) {
                                            mFileItemClickListener.onFileRemovedFromDevice(mAmazonFile);
                                        }
                                    }
                                });
                                builder.show();
                            }
                        });
                        break;
                    case R.id.completely_remove:
                        mainHandler.post(new Runnable() {
                            @Override
                            public void run() {

                                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

                                builder.setTitle(R.string.delete_header);
                                builder.setMessage(R.string.delete_local_message);
                                builder.setNegativeButton("Cancel", null);
                                builder.setPositiveButton("Delete", new DialogInterface.OnClickListener(){

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        new AsyncTask<String, Integer, Boolean>() {
                                            @Override
                                            protected Boolean doInBackground(String... strings) {
                                                mFileItemClickListener.onFileRemovedFromCloud(mAmazonFile);
                                                return true;
                                            }
                                        }.execute();
                                    }
                                });
                                builder.show();
                            }
                        });
                        break;
                    default:
                }
            }
        });

        BottomSheet fileActionsSheet = fileActionsSheetBuilder.build();



        if (mAmazonFile.isFavorite()) {
            fileActionsSheet.getMenu().removeItem(R.id.add_to_favourites);
        } else {
            fileActionsSheet.getMenu().removeItem(R.id.remove_favourites);
        }
        if (!mAmazonFile.isSynchronized()) {
            fileActionsSheet.getMenu().removeItem(R.id.remove_from_device);
        }

        fileActionsSheet.show();
    }

    public void setFileItemClickListener(@Nullable FileItemClickListener listener) {
        this.mFileItemClickListener = listener;
    }

    public interface FileItemClickListener {
        void onFileRemovedFromDevice (AmazonFile file);
        void onFileRemovedFromFavorites(AmazonFile file);
        void onFileAddedToFavorites(AmazonFile file);
        void onFileRemovedFromCloud(AmazonFile file);
    }

}

