package com.mive.android.socialstorageapplication.adapters;

import android.content.Context;
import android.os.Handler;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mive.android.socialstorageapplication.AmazonFilesComparator;
import com.mive.android.socialstorageapplication.DownloadingQueueObserver;
import com.mive.android.socialstorageapplication.R;
import com.mive.android.socialstorageapplication.enums.FileCategoryLoadEnum;
import com.mive.android.socialstorageapplication.enums.FileOperationTypeEnum;
import com.mive.android.socialstorageapplication.enums.FilesSortingModeEnum;
import com.mive.android.socialstorageapplication.listeners.ContextMenuClickListener;
import com.mive.android.socialstorageapplication.models.AmazonFile;
import com.mive.android.socialstorageapplication.models.SynchronizationMessage;
import com.mive.android.socialstorageapplication.services.AmazonFilesStorageService;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import at.grabner.circleprogress.CircleProgressView;

public class SocialStorageArrayAdapter extends ArrayAdapter<AmazonFile> {
    private final Context context;
    private List<AmazonFile> values;

    private ImageView optionsView;

    private FileCategoryLoadEnum mFileLoadCategoryType;

    private AmazonFilesStorageService mAmazonFilesStorageService;

    public SocialStorageArrayAdapter(Context context, ArrayList<AmazonFile> value, FileCategoryLoadEnum mFileLoadCategoryType) {
        super(context, R.layout.list_item_social_storage, value);

        this.context = context;
        this.values = value;
        this.mFileLoadCategoryType = mFileLoadCategoryType;
        this.mAmazonFilesStorageService = AmazonFilesStorageService.getInstance(context);
    }

    @Override
    public View getView(int position, final View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final View rowView = inflater.inflate(R.layout.list_item_social_storage, parent, false);

        final AmazonFile currentFile = values.get(position);

        TextView fileNameTextView = rowView.findViewById(R.id.fileNameLabel);
        TextView lastModifiedTextView = rowView.findViewById(R.id.lastModifiedLabel);

        ImageView imageView = rowView.findViewById(R.id.icon);
        final ImageView synchronizedImageView = rowView.findViewById(R.id.fileSynced);
        final ImageView favoriteImageView = rowView.findViewById(R.id.fileFavourite);
        optionsView = rowView.findViewById(R.id.moreDetails);

        if (DownloadingQueueObserver.isLoading(currentFile.getFilePath())) {
            CircleProgressView progressView = rowView.findViewById(R.id.fileDownloadProgress);
            progressView.setVisibility(View.VISIBLE);
            progressView.setSeekModeEnabled(false);
            progressView.setValueAnimated(DownloadingQueueObserver.getLoadingProgress(currentFile.getFilePath()));
        }

        if (currentFile.isSynchronized()) {
            synchronizedImageView.setVisibility(View.VISIBLE);
        }
        if (currentFile.isFavorite()) {
            favoriteImageView.setVisibility(View.VISIBLE);
        }

        fileNameTextView.setText(values.get(position).getFileName());
        lastModifiedTextView.setText(DateFormat.format("dd-MMM-yyyy", currentFile.getLastModified()).toString());

        ContextMenuClickListener listener = new ContextMenuClickListener(getContext(), getItem(position));
        listener.setFileItemClickListener(new ContextMenuClickListener.FileItemClickListener() {
            @Override
            public void onFileRemovedFromDevice(AmazonFile mAmazonFile) {

                if (mAmazonFilesStorageService.removeLocalFile(mAmazonFile.getFilePath())) {
                    EventBus.getDefault().post(new SynchronizationMessage(mAmazonFile, mFileLoadCategoryType, FileOperationTypeEnum.Remove));

                    if (mFileLoadCategoryType.equals(FileCategoryLoadEnum.LocalFilesOnly)) {
                        values.remove(mAmazonFile);
                    } else {
                        int index = values.indexOf(mAmazonFile);
                        mAmazonFile.setSynchronized(false);
                        values.set(index, mAmazonFile);
                        synchronizedImageView.setVisibility(View.GONE);
                    }

                    notifyDataSetChanged();

                    Toast.makeText(context, "Removed successfully", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Failed to remove", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFileRemovedFromFavorites(AmazonFile mAmazonFile) {
                if (mAmazonFilesStorageService.removeFavorite(mAmazonFile.getFilePath())) {

                    EventBus.getDefault().post(new SynchronizationMessage(mAmazonFile, mFileLoadCategoryType, FileOperationTypeEnum.NotFavorite));

                    if (mFileLoadCategoryType.equals(FileCategoryLoadEnum.FavoriteFilesOnly)) {
                        values.remove(mAmazonFile);
                    } else {
                        int index = values.indexOf(mAmazonFile);
                        mAmazonFile.setFavorite(false);
                        values.set(index, mAmazonFile);
                        favoriteImageView.setVisibility(View.GONE);
                    }

                    notifyDataSetChanged();

                    Toast.makeText(context, "Removed successfully", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(context, "Failed to remove", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFileAddedToFavorites(AmazonFile mAmazonFile) {
                if (mAmazonFilesStorageService.addFavoriteFile(mAmazonFile.getFilePath())) {

                    EventBus.getDefault().post(new SynchronizationMessage(mAmazonFile, mFileLoadCategoryType, FileOperationTypeEnum.Favorite));

                    int index = values.indexOf(mAmazonFile);
                    mAmazonFile.setFavorite(true);
                    values.set(index, mAmazonFile);
                    favoriteImageView.setVisibility(View.VISIBLE);
                    notifyDataSetChanged();

                    Toast.makeText(context, "Added to favorites", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Failed to add", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFileRemovedFromCloud(final AmazonFile mAmazonFile) {

                final boolean removed = mAmazonFilesStorageService.removeCloudFile(mAmazonFile.getFilePath());
                Handler mainHandler = new Handler(context.getMainLooper());
                mainHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (removed) {
                            EventBus.getDefault().post(new SynchronizationMessage(mAmazonFile, mFileLoadCategoryType, FileOperationTypeEnum.CompletelyRemove));

                            if (mFileLoadCategoryType.equals(FileCategoryLoadEnum.AllFiles)) {
                                values.remove(mAmazonFile);
                            } else {
                                int index = values.indexOf(mAmazonFile);
                                mAmazonFile.setSynchronized(false);
                                values.set(index, mAmazonFile);
                                synchronizedImageView.setVisibility(View.GONE);
                            }

                            notifyDataSetChanged();

                            Toast.makeText(context, "Removed successfully", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, "Failed to remove", Toast.LENGTH_SHORT).show();
                        }
                    }});
            }
        });

        optionsView.setOnClickListener(listener);
        imageView.setImageResource(R.drawable.ic_pdf_icon);

        return rowView;
    }

    public void setValues(List<AmazonFile> files) {
        values.clear();
        values.addAll(files);
        notifyDataSetChanged();
    }

    public void updateValue(int index, AmazonFile value) {
        values.set(index, value);
        notifyDataSetChanged();
    }

    public void sortFiles(FilesSortingModeEnum mode) {
        AmazonFilesComparator comparator = new AmazonFilesComparator(mode);
        sort(comparator);
        notifyDataSetChanged();
    }
}
