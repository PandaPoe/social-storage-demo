package com.mive.android.socialstorageapplication.services;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.util.Log;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.mive.android.socialstorageapplication.R;
import com.mive.android.socialstorageapplication.enums.FileCategoryLoadEnum;
import com.mive.android.socialstorageapplication.models.AmazonFile;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


public class AmazonFilesStorageService {

    private static final String SUFFIX = "/";

    private Context context;

    private AmazonS3 s3Client;
    private TransferUtility transferUtility;

    private TransferListener downloadProgressListener;

    private String defaultBucket;
    private List<String> downloadingQuery;

    @NonNull
    public static AmazonFilesStorageService getInstance(Context context) {
        return new AmazonFilesStorageService(context);
    }

    public AmazonFilesStorageService(Context context) {
        this.context = context;
        this.defaultBucket = context.getString(R.string.bucketName);
        this.downloadingQuery = new ArrayList<>();

        createS3Client();
    }


    public void setDownloadProgressListener(TransferListener transferListener) {
        this.downloadProgressListener = transferListener;
    }

    /**
     *  This method is used to Download the file to S3 by using transferUtility class
     **/
    public void downloadFileFromStorage(String fileNameKey){

        String root = Environment.getExternalStorageDirectory().toString();
        File synchronizedFilesDir = new File(root + "/synchronized_documents/");


        if (!synchronizedFilesDir.exists()) {
            synchronizedFilesDir.mkdir();
        }

        File localFilePath = new File(synchronizedFilesDir, fileNameKey);


        if (!downloadingQuery.contains(fileNameKey) && !getSavedFileNames().contains(fileNameKey)) {
            downloadingQuery.add(fileNameKey);

            TransferObserver transferObserver = transferUtility.download(
                    defaultBucket,     /* The bucket to download from */
                    fileNameKey,    /* The key for the object to download */
                    localFilePath        /* The file to download the object to */
            );

            if (downloadProgressListener != null) {
                transferObserver.setTransferListener(downloadProgressListener);
            }
        } else {
            openSavedFile(localFilePath.getAbsolutePath());
        }
    }

    public List<AmazonFile> getFilesFromStorage(FileCategoryLoadEnum categoryLoadEnum) {
        return getFilesFromStorage(defaultBucket, categoryLoadEnum);
    }

    public List<AmazonFile> getFilesFromStorage(String bucket, FileCategoryLoadEnum categoryLoadEnum) {

        List<AmazonFile> objectNames = new ArrayList<AmazonFile>();

        cleanupLocalStorage();

        if (categoryLoadEnum.equals(FileCategoryLoadEnum.LocalFilesOnly)) {
            return getLocalFiles(false);
        }

        try{
            Boolean isFileListRunning = true;

            FavoriteFilesStorageService helper = new FavoriteFilesStorageService(context);

            List<String> favoriteFiles = helper.getFavoriteFileNames();
            List<String> cachedFileNames = getSavedFileNames();

            String userFolder = context.getApplicationContext().getSharedPreferences("app_pref", context.MODE_PRIVATE).getString("user_id", "");

            Log.d("AMAZON", userFolder);

            ObjectListing objects = s3Client.listObjects(bucket, userFolder);
            Iterator<S3ObjectSummary> iterator = objects.getObjectSummaries().iterator();

            while (isFileListRunning) {
                while (iterator.hasNext()) {
                    S3ObjectSummary nextObject = iterator.next();
                    if (nextObject.getKey().endsWith(".pdf")) {
                        AmazonFile amazonFile = new AmazonFile(nextObject.getKey(), cachedFileNames.contains(nextObject.getKey()), nextObject.getLastModified(), nextObject.getSize());

                        boolean isFavorite = favoriteFiles.contains(nextObject.getKey());

                        amazonFile.setFavorite(isFavorite);

                        if (isFavorite && categoryLoadEnum.equals(FileCategoryLoadEnum.FavoriteFilesOnly) || !categoryLoadEnum.equals(FileCategoryLoadEnum.FavoriteFilesOnly))
                            objectNames.add(amazonFile);
                    }
                }

                isFileListRunning = objects.isTruncated();

                if (isFileListRunning) {
                    objects = s3Client.listNextBatchOfObjects(objects);
                    iterator = objects.getObjectSummaries().iterator();
                }
            }
        } catch (Exception ex) {
            if (categoryLoadEnum.equals(FileCategoryLoadEnum.FavoriteFilesOnly))
                return getLocalFiles(true);
            return new ArrayList<>();
        }

        return objectNames;
    }

    public List<AmazonFile> getLocalFiles(boolean onlyFavorite) {

        List<AmazonFile> objectNames = new ArrayList<AmazonFile>();

        try {
            FavoriteFilesStorageService helper = new FavoriteFilesStorageService(context);

            List<String> favoriteFiles = helper.getFavoriteFileNames();

            String root = Environment.getExternalStorageDirectory().toString();


            String userFolder = context.getApplicationContext().getSharedPreferences("app_pref", context.MODE_PRIVATE).getString("user_id", "");
            File synchronizedFilesDir = new File(root + "/synchronized_documents/" + userFolder);


//            SharedPreferences prefs = context.getApplicationContext().getSharedPreferences("app_pref", context.MODE_PRIVATE);
//
//            if (prefs.getBoolean("firstrun", true)) {
//                deleteRecursive(synchronizedFilesDir);
//                context.getApplicationContext().getSharedPreferences("app_pref", context.MODE_PRIVATE).edit().putBoolean("firstrun", false).commit();
//                return objectNames;
//            }

            if (synchronizedFilesDir.exists()) {

                File[] folderFiles = synchronizedFilesDir.listFiles();
                for (File folderFile : folderFiles) {

                    String filePath = userFolder + SUFFIX + folderFile.getName();
                    boolean isFavorite = favoriteFiles.contains(filePath);

                    AmazonFile localFile = new AmazonFile();
                    localFile.setFileName(folderFile.getName());

                    localFile.setFilePath(filePath);
                    localFile.setLastModified(new Date(folderFile.lastModified()));
                    localFile.setSynchronized(true);
                    localFile.setFavorite(isFavorite);

                    if (onlyFavorite && isFavorite || !onlyFavorite)
                        objectNames.add(localFile);
                }
            }

        } catch (Exception ex) {
            objectNames = new ArrayList<>();
        }

        return objectNames;
    }

    public boolean removeCloudFile(String fileName) {
        try {
            removeLocalFile(fileName);
            removeFavorite(fileName);
            s3Client.deleteObject(defaultBucket, fileName);

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public boolean removeLocalFile(String fileName) {
        try {
            String root = Environment.getExternalStorageDirectory().toString();
            File synchronizedFilesDir = new File(root + "/synchronized_documents/" + fileName);
            return synchronizedFilesDir.delete();
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean addFavoriteFile(String fileName) {
        try {
            FavoriteFilesStorageService helper = new FavoriteFilesStorageService(context);
            long newFileId = helper.addFavoriteFile(fileName);
            return newFileId > 0;
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean removeFavorite(String fileName) {
        try {
            FavoriteFilesStorageService helper = new FavoriteFilesStorageService(context);
            helper.removeFavoriteFile(fileName);
            return true;
        } catch (Exception ex) {
            return true;
        }
    }

    private List<String> getSavedFileNames() {
        String root = Environment.getExternalStorageDirectory().toString();

        String userFolder = context.getApplicationContext().getSharedPreferences("app_pref", context.MODE_PRIVATE).getString("user_id", "");

        File synchronizedFilesDir = new File(root + "/synchronized_documents/" + userFolder);

        List<String> folderFileNames = new ArrayList<>();

        if (synchronizedFilesDir.exists()) {
            File[] folderFiles = synchronizedFilesDir.listFiles();
            for (File folderFile : folderFiles) {
                folderFileNames.add(userFolder + SUFFIX + folderFile.getName());
            }
        }
        return folderFileNames;
    }

    private void openSavedFile(String filePath) {
        Intent intent = new Intent(Intent.ACTION_VIEW);

        Uri apkURI = FileProvider.getUriForFile(
                context,
                "com.mive.android.socialstorageapplication.provider",
                new File(filePath));

        intent.setDataAndType(apkURI, "application/pdf");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        context.startActivity(intent);
    }

    public boolean createFolder(String folderName) {
        try {
            // create meta-data for your folder and set content-length to 0
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength(0);
            // create empty content
            InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
            // create a PutObjectRequest passing the folder name suffixed by /
            PutObjectRequest putObjectRequest = new PutObjectRequest(defaultBucket,
                    folderName + SUFFIX, emptyContent, metadata);

            // send request to S3 to create folder
            s3Client.putObject(putObjectRequest);
            return true;
        }
        catch(AmazonServiceException e) {
        // The call was transmitted successfully, but Amazon S3 couldn't process
        // it, so it returned an error response.
            e.printStackTrace();
            return false;
        }
        catch(AmazonClientException e) {
        // Amazon S3 couldn't be contacted for a response, or the client
        // couldn't parse the response from Amazon S3.
            e.printStackTrace();
            return false;
        }
        catch(Exception e) {
            //In case when unknown exception has been occurred
            e.printStackTrace();
            return false;
        }
    }

    public boolean removeFolder(String folderName) {
        try {
            s3Client.deleteObject(defaultBucket, folderName);
            return true;
        }
        catch(AmazonServiceException e) {
            // The call was transmitted successfully, but Amazon S3 couldn't process
            // it, so it returned an error response.
            e.printStackTrace();
            return false;
        }
        catch(AmazonClientException e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
            e.printStackTrace();
            return false;
        }
        catch(Exception e) {
            //In case when unknown exception has been occurred
            e.printStackTrace();
            return false;
        }
    }

    public void openDownloadedFile(String fileName) {
        String root = Environment.getExternalStorageDirectory().toString()+ "/synchronized_documents/" + fileName;
        openSavedFile(root);
    }

    private CognitoCachingCredentialsProvider getStorageCredentialsProvider() {

        // Initialize the AWS Credential
        CognitoCachingCredentialsProvider cognitoCachingCredentialsProvider =
                new CognitoCachingCredentialsProvider(
                        context,
                        context.getString(R.string.identityPoolId),
                        Regions.US_EAST_1);
        return cognitoCachingCredentialsProvider;
    }


    private void createS3Client() {
        CognitoCachingCredentialsProvider credentialsProvider = getStorageCredentialsProvider();

        s3Client = new AmazonS3Client(credentialsProvider);
        s3Client.setRegion(Region.getRegion(Regions.US_EAST_1));

        transferUtility = TransferUtility.builder().s3Client(s3Client).context(context).build();
    }

    private void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);

        fileOrDirectory.delete();
    }

    private void cleanupLocalStorage() {

        SharedPreferences prefs = context.getApplicationContext().getSharedPreferences("app_pref", context.MODE_PRIVATE);

        if (prefs.getBoolean("firstrun", true)) {

            String root = Environment.getExternalStorageDirectory().toString();

            String userFolder = context.getApplicationContext().getSharedPreferences("app_pref", context.MODE_PRIVATE).getString("user_id", "");

            File synchronizedFilesDir = new File(root + "/synchronized_documents/" + userFolder);

            deleteRecursive(synchronizedFilesDir);
            context.getApplicationContext().getSharedPreferences("app_pref", context.MODE_PRIVATE).edit().putBoolean("firstrun", false).commit();
        }
    }
}

