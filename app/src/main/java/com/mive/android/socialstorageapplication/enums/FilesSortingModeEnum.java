package com.mive.android.socialstorageapplication.enums;

public enum FilesSortingModeEnum {
    AscendingByDate,
    DescendingByDate,
    AscendingByName,
    DescendingByName
}
