package com.mive.android.socialstorageapplication.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.mive.android.socialstorageapplication.enums.FileCategoryLoadEnum;
import com.mive.android.socialstorageapplication.fragments.SocialStorageContentFragment;

public class SectionsPagerAdapter extends FragmentPagerAdapter {

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return SocialStorageContentFragment.newInstance(FileCategoryLoadEnum.getEnumById(position));
    }

    @Override
    public int getCount() {
        return 3;
    }
}
