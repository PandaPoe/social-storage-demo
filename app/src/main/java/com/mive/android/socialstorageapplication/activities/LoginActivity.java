package com.mive.android.socialstorageapplication.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.mive.android.socialstorageapplication.R;
import com.mive.android.socialstorageapplication.services.AmazonFilesStorageService;

import java.util.Arrays;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button fb, google;
    private TextView signup;
    private SignInButton signInButton;
    private LoginButton loginButton;
    private static final int RC_SIGN_IN = 1993;
    private FirebaseAuth mAuth;
    private static final String EMAIL = "email";
    private static final String TAG = "LoginActivity";

    private SharedPreferences app_pref;

    CallbackManager callbackManager;
    GoogleSignInClient mGoogleSignInClient;
    AmazonFilesStorageService amazonFilesStorageService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //app_pref = getSharedPreferences("app_pref", MODE_PRIVATE);

        amazonFilesStorageService = AmazonFilesStorageService.getInstance(this);


        //Real Buttons - UI
        google = findViewById(R.id.google);
        fb = findViewById(R.id.fb);
        signup = findViewById(R.id.signUp);

        //Init Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        //Google Button
        signInButton = findViewById(R.id.sign_in_button);
        signInButton.setOnClickListener(this);

        //Facebook callback manager
        callbackManager = CallbackManager.Factory.create();

        //Facebook Button
        loginButton = findViewById(R.id.login_button);

        //Set read permission
        loginButton.setReadPermissions(Arrays.asList(EMAIL));

        //Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());

            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
                Snackbar.make(findViewById(R.id.login_layout), "Login Cancelled!", Snackbar.LENGTH_LONG).show();

            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
                Snackbar.make(findViewById(R.id.login_layout), "Login Cancelled!", Snackbar.LENGTH_LONG).show();

            }
        });

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
                updateUI(null);
                // ...
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            final FirebaseUser Googleuser = mAuth.getCurrentUser();
                            final String userID = Googleuser.getUid();

                            getApplicationContext().getSharedPreferences("app_pref", MODE_PRIVATE).edit().putString("user_id", userID).commit();
                            Log.d(TAG, getApplicationContext().getSharedPreferences("app_pref", MODE_PRIVATE).getString("user_id", ""));
                            // create folder into bucket

                            //boolean isNewGoogleUser = task.getResult().getAdditionalUserInfo().isNewUser();
                            //updateUI(Googleuser);
                            //If the account is newly created

                            new AsyncTask<String, Integer, Boolean>() {
                                @Override
                                protected Boolean doInBackground(String... strings) {
                                    return amazonFilesStorageService.createFolder(userID);
                                }

                                @Override
                                protected void onPostExecute(Boolean aBoolean) {
                                    if (aBoolean) {
                                        Toast.makeText(getApplicationContext(), "Your account has been successfully created. ", Toast.LENGTH_SHORT).show();
                                        updateUI(Googleuser);
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Error while creating an account. ", Toast.LENGTH_SHORT).show();
                                    }

                                    super.onPostExecute(aBoolean);
                                }

                                @Override
                                protected void onCancelled(Boolean aBoolean) {
                                    super.onCancelled(aBoolean);
                                }
                            }.execute();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Snackbar.make(findViewById(R.id.login_layout), "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
                            updateUI(null);
                        }
                    }
                });
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }

    private void updateUI(FirebaseUser user) {
        if (user != null) {
            Intent intent = new Intent(getApplicationContext(), SocialFeedActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            final FirebaseUser Fbuser = mAuth.getCurrentUser();
                            final String userID = Fbuser.getUid();
                            // create folder into bucket

                            app_pref.edit().putString("user_id", userID).commit();
                            Log.d(TAG, userID);

                            new AsyncTask<String, Integer, Boolean>() {
                                @Override
                                protected Boolean doInBackground(String... strings) {
                                    return amazonFilesStorageService.createFolder(userID);
                                }

                                @Override
                                protected void onPostExecute(Boolean aBoolean) {
                                    if (aBoolean) {
                                        Toast.makeText(getApplicationContext(), "Your account has been successfully created. ", Toast.LENGTH_SHORT).show();
                                        updateUI(Fbuser);
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Error while creating an account. ", Toast.LENGTH_SHORT).show();
                                    }

                                    super.onPostExecute(aBoolean);
                                }

                                @Override
                                protected void onCancelled(Boolean aBoolean) {
                                    super.onCancelled(aBoolean);
                                }
                            }.execute();

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });
    }


    @Override
    public void onClick(View v) {
        if (v == fb) {
            loginButton.performClick();
        } else if (v == google) {
            signIn();

        }
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

}
