package com.mive.android.socialstorageapplication;

import java.util.HashMap;
import java.util.Map;

public class DownloadingQueueObserver {
    private static Map<String, Integer> queue = new HashMap<>();

    public static void set(String fileName, int progress) {
        queue.put(fileName, progress);
    }

    public static boolean isLoading(String fileName) {
        return queue.containsKey(fileName);
    }

    public static int getLoadingProgress(String fileName) {
        if(queue.containsKey(fileName)) {
            return queue.get(fileName);
        }
        return -1;
    }

    public static void remove (String fileName) {
        queue.remove(fileName);
    }
}
