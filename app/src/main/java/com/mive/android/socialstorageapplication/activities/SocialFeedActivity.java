package com.mive.android.socialstorageapplication.activities;

import android.Manifest;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import android.support.v7.widget.Toolbar;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.mive.android.socialstorageapplication.R;
import com.mive.android.socialstorageapplication.fragments.SocialFeedFragment;
import com.mive.android.socialstorageapplication.fragments.SocialStorageFragment;

public class SocialFeedActivity extends AppCompatActivity {

    private Toolbar toolBar;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            switch (item.getItemId()) {
                case R.id.navigation_home:
                    toolBar.setTitle(R.string.title_home);
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.contentView, SocialFeedFragment.newInstance())
                            .commitNow();
                    return true;
                case R.id.navigation_dashboard:
                    toolBar.setTitle(R.string.title_social_storage);
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.contentView, SocialStorageFragment.newInstance())
                            .commitNow();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social_feed);

        toolBar = findViewById(R.id.toolbar);
        setSupportActionBar(toolBar);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        if (savedInstanceState == null) {
            getSupportActionBar().setTitle(getString(R.string.title_home));
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.contentView, SocialFeedFragment.newInstance())
                    .commitNow();
        }

        checkExternalStorageWritingPermission();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 1984) {
            if (grantResults.length >= 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted
            } else {
                // permission wasn't granted
            }
        }
    }

    private void checkExternalStorageWritingPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                // permission wasn't granted
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1984);
            }
        }
    }

    private static boolean canWriteToExternalStorage(Context context) {
        return ContextCompat.checkSelfPermission(context, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }
}
