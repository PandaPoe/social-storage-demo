package com.mive.android.socialstorageapplication.enums;

public enum FileCategoryLoadEnum {
    AllFiles(0),
    LocalFilesOnly(1),
    FavoriteFilesOnly(2);

    FileCategoryLoadEnum(int i) {

    }

    public static FileCategoryLoadEnum getEnumById (int id) {
        switch (id) {
            case 0:
                return AllFiles;
            case 1:
                return LocalFilesOnly;
            case 2:
                return FavoriteFilesOnly;
            default:
                return AllFiles;
        }
    }
}
