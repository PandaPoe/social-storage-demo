package com.mive.android.socialstorageapplication.listeners;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.cocosw.bottomsheet.BottomSheet;
import com.mive.android.socialstorageapplication.R;

public class ToolbarMenuClickListener implements MenuItem.OnMenuItemClickListener {
    private Context mContext;

    private SortingItemClickListener mSortingItemClickListener;

    public ToolbarMenuClickListener (Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item)  {
        new BottomSheet.Builder(mContext, R.style.BottomSheet_Dialog)
                .title("Sort by")
                .sheet(R.menu.sorting_menu)
                .listener(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Handler mainHandler = new Handler(mContext.getMainLooper());
                        switch (which) {
                            case R.id.name_asc:
                                mainHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (mSortingItemClickListener!=null)
                                            mSortingItemClickListener.sortByNameAscending();
                                    }
                                });
                                break;

                            case R.id.name_desc:
                                mainHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (mSortingItemClickListener!=null) {
                                            mSortingItemClickListener.sortByNameDescending();
                                        }
                                    }
                                });
                                break;

                            case R.id.date_asc:
                                mainHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (mSortingItemClickListener!=null) {
                                            mSortingItemClickListener.sortByDateAscending();
                                        }
                                    }
                                });
                                break;
                            case R.id.date_desc:
                                mainHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (mSortingItemClickListener!=null) {
                                            mSortingItemClickListener.sortByDateDescending();
                                        }
                                    }
                                });
                                break;
                            default:
                                return;
                        }
                    }
                }).show();
        return true;
    }

    public void setSortingItemClickListener(@Nullable SortingItemClickListener listener) {
        this.mSortingItemClickListener = listener;
    }


    public interface SortingItemClickListener {
        void sortByNameAscending();
        void sortByNameDescending();
        void sortByDateAscending();
        void sortByDateDescending();
    }
}
